package com.kameocode.tasks.infra;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Data
@Accessors(fluent = true)
@RequiredArgsConstructor
public class ProducerConsumerSubsystem<T> {
    @Setter
    private int consumerCount = 1;

    @Setter
    private int producerCount = 1;

    @Setter
    private int queueMaximum = 10;

    @Setter
    private int queueMinimum = 1;

    @Setter
    private long waitPeriodTimeInMillis = 100L;

    @Setter
    private TaskExecutor consumersPool;

    @Setter
    private TaskExecutor producersPool;

    @Setter
    private Supplier<BlockingQueue<T>> blockingQueueProvider = LinkedBlockingQueue::new;

    private final Supplier<Supplier<T>> producerProvider;
    private final Supplier<Consumer<T>> consumerProvider;

    public ProducerConsumerSubsystem(Supplier<T> producer, Consumer<T> consumer) {
        this.producerProvider = () -> producer;
        this.consumerProvider = () -> consumer;
    }


    public void start() {
        if (waitPeriodTimeInMillis < 1) {
            throw new IllegalArgumentException("waitPeriodTimeInMillis should be > 0");
        }

        BoundedQueueWrapper<T> queueWrapper = new BoundedQueueWrapper<>(blockingQueueProvider.get(), queueMinimum, queueMaximum);
        TaskExecutor consumersPool = Optional.ofNullable(this.consumersPool).orElseGet(this::newConsumersPoolTaskExecutor);
        TaskExecutor producersPool = Optional.ofNullable(this.producersPool).orElseGet(this::newProducersPoolTaskExecutor);

        Stream.generate(() -> newBaseConsumer(queueWrapper))
                .limit(consumerCount)
                .forEach(consumersPool::execute);

        Stream.generate(() -> newBaseProducer(queueWrapper))
                .limit(producerCount)
                .forEach(producersPool::execute);

    }

    private BaseConsumer<T> newBaseConsumer(BoundedQueueWrapper<T> queueWrapper) {
        return new BaseConsumer<>(queueWrapper, consumerProvider.get(), waitPeriodTimeInMillis);
    }

    private BaseProducer<T> newBaseProducer(BoundedQueueWrapper<T> queueWrapper) {
        return new BaseProducer<>(queueWrapper, producerProvider.get());
    }


    private TaskExecutor newConsumersPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(consumerCount);
        executor.setMaxPoolSize(consumerCount);
        executor.setThreadNamePrefix("consumers-");
        executor.initialize();
        return executor;
    }

    private TaskExecutor newProducersPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(producerCount);
        executor.setMaxPoolSize(producerCount);
        executor.setThreadNamePrefix("producers-");
        executor.initialize();
        return executor;
    }
}
