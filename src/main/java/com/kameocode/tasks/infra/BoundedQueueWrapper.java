package com.kameocode.tasks.infra;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.String.format;


@Slf4j
public class BoundedQueueWrapper<T> {
    private final BlockingQueue<T> queue;
    private final int maxQueue;
    private final int minQueue;

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition wantMore = lock.newCondition();


    public BoundedQueueWrapper(BlockingQueue<T> queue, int minQueue, int maxQueue) {
        if (minQueue > maxQueue) {
            throw new IllegalArgumentException(format("MinQueue (%s) should be less than or equal to maxQueue (%s)",
                    minQueue, maxQueue));
        }
        this.queue = queue;
        this.maxQueue = maxQueue;
        this.minQueue = minQueue;
    }


    public T poll(long consumerWaitPeriodInMillis) throws InterruptedException {
        T poll = queue.poll(consumerWaitPeriodInMillis, TimeUnit.MILLISECONDS);
        lock.lock();
        try {
            int size = queue.size();
            log.trace("#### consumed task {}, queue size = {} ", poll, size);
            if (size < minQueue) {
                this.wantMore.signal();
            }
            return poll;
        } finally {
            lock.unlock();
        }
    }

    public void put(T task) throws InterruptedException {
        lock.lock();
        if (queue.size() >= maxQueue) {
            // wait for conumers to consume sufficient number of tasks
            this.wantMore.await();
        }
        queue.put(task);
        log.trace("#### produced task, queue size = {} ", queue.size());
        if (queue.size() < maxQueue) {
            this.wantMore.signal();
        }
        lock.unlock();
    }

    public int size() {
        return queue.size();
    }


}
