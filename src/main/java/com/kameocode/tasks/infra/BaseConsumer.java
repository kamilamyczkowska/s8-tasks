package com.kameocode.tasks.infra;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;


@Slf4j
@RequiredArgsConstructor
public class BaseConsumer<T> implements Runnable {

    private final BoundedQueueWrapper<T> queue;
    private final Consumer<T> consumer;
    private final long consumerWaitPeriodInMillis;
    private volatile boolean running;

    @SneakyThrows(InterruptedException.class)
    public void run() {
        this.running = true;
        while (running) {
            T task = queue.poll(consumerWaitPeriodInMillis);
            if (task == null) {
                log.trace("No task found");
            } else {
                log.trace("Consuming task {}", task);
                consumer.accept(task);
            }
        }
    }

    public void terminate() {
        this.running = false;
    }

}
