package com.kameocode.tasks.infra;


import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Supplier;


@Slf4j
@RequiredArgsConstructor
public class BaseProducer<T> implements Runnable {
    private final BoundedQueueWrapper<T> queue;
    public final Supplier<T> supplier;
    private volatile boolean running;

    @Override
    @SneakyThrows(InterruptedException.class)
    public void run() {
        this.running = true;
        while (running) {
            T task = supplier.get();
            log.trace("Producing task {}", task);
            queue.put(task);
        }
    }

    public void terminate() {
        this.running = false;
    }
}

