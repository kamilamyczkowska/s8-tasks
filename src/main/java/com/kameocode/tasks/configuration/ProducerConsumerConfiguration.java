package com.kameocode.tasks.configuration;

import com.kameocode.tasks.domain.AdditionTask;
import com.kameocode.tasks.infra.ProducerConsumerSubsystem;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Valid
@Configuration
@ConfigurationProperties(prefix = "tasks")
@Data
public class ProducerConsumerConfiguration {

    @Min(1)
    private int consumerCount;

    @Min(1)
    private int producerCount;

    @Min(1)
    private int queueMaximum;

    @Min(1)
    private int queueMinimum;

    @Min(1)
    private long waitPeriodTimeInMillis;

    @Bean
    ProducerConsumerSubsystem<AdditionTask> newSubsystem() {
        Consumer<AdditionTask> consumer = AdditionTask::run;
        Supplier<AdditionTask> supplier = AdditionTask::newRandomAdditionTask;
        return new ProducerConsumerSubsystem<>(supplier, consumer)
                .consumerCount(consumerCount)
                .producerCount(producerCount)
                .queueMaximum(queueMaximum)
                .queueMinimum(queueMinimum)
                .waitPeriodTimeInMillis(waitPeriodTimeInMillis);
    }


}


