package com.kameocode.tasks.domain;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@ToString
@Slf4j
public class AdditionTask implements Runnable {
    private final double firstNumber;
    private final double secondNumber;

    @Override
    public void run() {
        // sleep is for pretending blocking action in order to avoid proc overheating ;)
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        double sum = firstNumber + secondNumber;
        log.info(String.format("%s + %s = %s", firstNumber, secondNumber, sum));
    }

    public static AdditionTask newRandomAdditionTask() {
        try {
            Thread.sleep(10L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new AdditionTask(Math.random(), Math.random());
    }
}
