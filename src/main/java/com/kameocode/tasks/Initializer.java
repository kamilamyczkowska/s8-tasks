package com.kameocode.tasks;

import com.kameocode.tasks.infra.ProducerConsumerSubsystem;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class Initializer {


    @Autowired
    private final ProducerConsumerSubsystem subsystem;

    @PostConstruct
    public void init() {
        subsystem.start();
    }
}
