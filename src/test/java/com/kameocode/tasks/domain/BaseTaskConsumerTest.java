package com.kameocode.tasks.domain;

import com.kameocode.tasks.infra.BaseConsumer;
import com.kameocode.tasks.infra.BoundedQueueWrapper;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class BaseTaskConsumerTest {

    @Test
    public void shouldConsumeTask() throws InterruptedException {

        // given: producers created three tasks
        BoundedQueueWrapper<AdditionTask> queue = mock(BoundedQueueWrapper.class);
        AdditionTask task1 = mock(AdditionTask.class);
        AdditionTask task2 = mock(AdditionTask.class);
        AdditionTask task3 = mock(AdditionTask.class);
        when(queue.poll(100L)).thenReturn(null, task1, task2, task3, null);
        Consumer<AdditionTask> consumer = AdditionTask::run;

        // when: we start consuming tasks
        BaseConsumer<AdditionTask> taskConsumer = new BaseConsumer<>(queue, consumer, 100L);
        new Thread(taskConsumer).start();
        new Thread(taskConsumer).start();
        new Thread(taskConsumer).start();


        try {
            TimeUnit.SECONDS.sleep(1L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // then: after two seconds all three tasks are consumed
        verify(queue, atLeast(5)).poll(100L);
        verify(task1).run();
        verify(task2).run();
        verify(task3).run();

    }

}