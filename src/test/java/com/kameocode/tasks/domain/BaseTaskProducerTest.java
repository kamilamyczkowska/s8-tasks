package com.kameocode.tasks.domain;

import com.kameocode.tasks.infra.BaseConsumer;
import com.kameocode.tasks.infra.BaseProducer;
import com.kameocode.tasks.infra.BoundedQueueWrapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class BaseTaskProducerTest {

    @Test
    void shouldProduceTasks() {

        // given: queue and producers
        BoundedQueueWrapper<AdditionTask> queue = new BoundedQueueWrapper<>(new LinkedBlockingQueue<>(), 5, 10);

        Supplier<AdditionTask> supplier = () -> new AdditionTask(1, 1);
        BaseProducer<AdditionTask> taskProducer = new BaseProducer(queue, supplier);

        // when: we start producing tasks
        new Thread(taskProducer).start();
        new Thread(taskProducer).start();

        try {
            TimeUnit.SECONDS.sleep(2L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // then: after two seconds exactly maxQueue tasks were created
        Assertions.assertEquals(10, queue.size());
    }


    @Test
    void shouldProduceAndConsumeTasksInParallel() {

        // given: queue and slow producers and consumers (1 second each)
        BoundedQueueWrapper<AdditionTask> queue = new BoundedQueueWrapper<>(new LinkedBlockingQueue<>(), 5, 10);
        Consumer<AdditionTask> consumer = mock(Consumer.class);
        Supplier<AdditionTask> supplier = mock(Supplier.class);

        doAnswer((Answer<Void>) invocationOnMock -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }).when(consumer).accept(any(AdditionTask.class));

        when(supplier.get()).thenAnswer((Answer<AdditionTask>) invocationOnMock -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return new AdditionTask(1, 1);
        });
        BaseProducer<AdditionTask> taskProducer = new BaseProducer<>(queue, supplier);
        BaseConsumer<AdditionTask> taskConsumer = new BaseConsumer<>(queue, consumer, 100L);


        // when: we start producing tasks on four separate threads
        // when: we start consuming tasks on four separate threads
        for (int i = 0; i < 4; i++) {
            new Thread(taskProducer).start();
            new Thread(taskConsumer).start();
        }


        // then: after three seconds at least 8 tasks should be processed
        try {
            TimeUnit.SECONDS.sleep(3L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        verify(consumer, atLeast(8)).accept(any());
        verify(supplier, atLeast(8)).get();
    }
}